module.exports = {
    apps: [
      {
        name: "auth",
        script: "npm",
        args: "run start",
        cwd: "./auth-micro",
        watch: false
      },
      {
        name: "order",
        script: "npm",
        args: "run start",
        cwd: "./order-micro",
        watch: false
      },
      {
        name: "product",
        script: "npm",
        args: "run start",
        cwd: "./product-micro",
        watch: false
      },
      {
        name: "gateway",
        script: "npm",
        args: "run start",
        cwd: "./gateway-micro",
        watch: false
      }
    ]
  };
  