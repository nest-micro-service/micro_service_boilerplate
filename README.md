## Description
  
Microservices with TypeScript, gRPC, API Gateway, and Authentication | by Ahmad Alhourani

## Repositories

- https://gitlab.com/nest-micro-service/micro_service_boilerplate - App Boilerplate

- https://gitlab.com/nest-micro-service/product-micro - Product Micro (gRPC)
- https://gitlab.com/nest-micro-service/order-micro - Order Micro (gRPC)
- https://gitlab.com/nest-micro-service/auth-micro  - Authentication Micro (gRPC)

- https://gitlab.com/nest-micro-service/gateway-micro - API Gateway (HTTP)
- https://gitlab.com/nest-micro-service/grpc-proto - Shared Proto Repository

## Pre Installation
copy ssh_config.example to ssh_config  and update ur ssh key connection

```bash
 cp  -f ssh_config.example ssh_config
```






```bash
"grpc-proto": "gitlab@gitlab.com-ahmad:nest-micro-service/grpc-proto",
```
   to
```bash
 "grpc-proto": "GIT_SSH_COMMAND='ssh -F ./../ssh_config' git+ssh://git@gitlab.com:nest-micro-service/grpc-proto.git",
```


## Installation


```bash
sudo snap install protobuf  --classic
sudo apt install -y protobuf-compiler
```

# load submodules
```bash
git submodule update --init --recursive 
```

## Run

copy ./shared-env/example.env to ./shared-env/development.env

```bash
 cp  -f ./shared-env/example.env  ./shared-env/development.env
```
and update your environment


# install dep and Run
```bash
$ npm run refresh:all

$ pm2 start ecosystem.config.js

```


# Test
use [micro_service_boilerplate.postman_collection](https://gitlab.com/nest-micro-service/micro_service_boilerplate/-/blob/master/micro_service_boilerplate.postman_collection)


## Reference
- [NestJS: Microservices with gRPC](https://levelup.gitconnected.com/nestjs-microservices-with-grpc-api-gateway-and-authentication-part-1-2-650009c03686)
- [gRPC vs. REST](https://blog.dreamfactory.com/grpc-vs-rest-how-does-grpc-compare-with-traditional-rest-apis/#:~:text=Here%20are%20the%20main%20differences,usually%20leverages%20JSON%20or%20XML.)
- [Git Tools - Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

## Author

- [Ahmad Alhourani](https://www.linkedin.com/in/ahmad-alhourani)
